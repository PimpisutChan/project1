# Project 1 Description : Pac-man Game #

Link for this Project GIT Repository [here](https://bitbucket.org/PimpisutChan/project1/src/master/)

### Table of content

* [Source code](Project1_Code/)
* Demo Video

[![](Images/PlayScreen.png)](Video/)

* Description of projects and notes in README.md (this file). 
	
---
### Objective

This program is an another version of Pac-man Game. The player controls movement of Pac-man through an enclosed maze via arrow keys; objective of this Game is to eat four colored ghosts - Blinky(red), Pinky(pink), Inky(cyan), and Clyde(orange). To eat each of ghosts, player has to collect certain amount of specific fruits, then press a specific key to upgrade Pac-man. 

---
### How to play the Game

* Press 'p' to start the Game
* Yellow Pac-man is a default state which is not be able to eat any ghosts and if it contacts any ghosts, it will die.
* Player controls Pac-man with arrow keys. When player press an arrow key, Pac-man will move in a corresponding direction until there is a wall or player press another arrow key to change direction. Pac-man has to collect certain number of fruits to be able to eat corresponding ghost. For instance, Pac-man collected 2 apples and press 'a', then Pac-man is upgraded to red version and it can eat Blinky, red ghost. The color of upgraded Pac-man has to match ghost's color that Pac-man eats, otherwise Pac-man will die.

![](Images/Guide_table.png)

* Player will get +20 points for collecting one fruit item, and +100 points for eating one ghost.

* Player has 3 extra lives. Once Pac-man dies, an extra live will be used. When Pac-man is dead and there is no extra lives left, then the game will be over, and player loses. On the other hand, if all ghosts are eaten, player wins.

* If the Game ends, press 'p' to restart the game.

---
### Processing Library

*Sprite library* 

**Hightlight Methods**

* `setFrameSequence(int firstFrame, int lastFrame, double interval)` Sets upt the animation sequence

* `update(double deltaTime)`  Update the positions of all the sprites

* `setVisible(boolean visible)` Set the sprite visibility

* `setSpeed(double speed, double angle)` Set the sprites speed and direction of travel

* `oo_collision(Sprite spriteB, float pcent)` Collision detection based on the percentage overlap of THIS sprite caused by spriteB 

---
### Character Implemetation

#### Ghost

Characterization of Ghost are

1. Change direction overtime to have *random movement*
2. All ghosts has different speed

![](Images/Ghost_UML.png)

All ghost objects is added to global ArrayList *ghosts*

**Hightlight method**

`run()` When a ghost's location is at the center of a sqaure grid, this method is called to detect 4 pairs of coordinates of adjacent squares. Then, a ghost will move randomly in a direction among possible directions. Once the ghost reach center of next square grid, this method will be called again. 

![](Images/Ghost_detection.png)

Note: I had decided to use Thread(i.e. implements Runnable) and `Thread.sleep()`. However, becuase of having 1 Thread per 1 Sprite, and 4 Threads in total, the output screen had *flashing light* while the programming was running.

So later, instead of using Thread, I implemented normal stopwatchs(each ghost has different lapping time), and the program can run effectively.

#### Pac-man

Characterization of Pac-man are 

1. Control direction by arrow keys
2. Speed is set to zero when it contacts walls

**Hightlight method**

* `noPressed()` Check avalability of next movement at current direction. It checks two pairs of coordinate whether they are available or not, beacuse player can press arrow keys whenever player wants, so one pair detection does not work in this case.

![](Images/Pacman_detection.png)

#### Fruit

![](Images/Fruit_UML.png)

All fruit  objects is added to global ArrayList *fruits*. Randomly place all fruits in ArrayList *fruits* on possible locations of the board when the game is started.

#### Pac-man vs Ghost 

What I would like to do is when there is a contact between Pac-man and a ghost, one of them should be triggered to display dying frames (the sequence must happen only 1 loop) and eventually be invisible. `isInvisible(true)` cannot be implemented, becuase it sets the visibility immediately. So, I implementent new method `isDying()` in class *Ghost* and *Pacman* which triggers to display dying frames, and when the number indicating last frame is detected, then set the object to be invisible by library method.

**Hightlight code**

A method in class Ghost
```java
public void isDying()
{
    setSpeed(0);
    setFrameSequence(8, 12, 0.2f);
    dying = true;
}
```

Codes in `processCollisions()` method
```java
for(int i = 0; i < ghosts.size(); i++){
    if (player.oo_collision(ghosts.get(i), 20) && !ghosts.get(i).getDying() && !player.getDying()) {
        if (player.getState() == (i+1)%5) {
            ghosts.get(i).isDying();
            score += 100;
            numDead += 1;
        }
        else player.isDying();
    }
}
```

Codes in `draw()` method. Set the ghost to be invisible when last frame of dying frames is detected.

```java
for (Ghost ghost: ghosts) 
{
    if (ghost.getFrame() == 12) ghost.setVisible(false);
    ghost.draw();
}
```

#### Pac-man vs Fruit

If there is an overlap between Pac-man and a fruit item more than 60 percent, the fruit is collected.

```java
for (Fruit fruit: fruits){
    if (player.oo_collision(fruit, 60) && fruit.isVisible()){
        // TODO
    }
}
```

---
### Code Inspiration

1. Examples provided by Processing Contributed Libraries (Sprites/S4P_TankDemo) 

    * Good example of how Sprite could be used in a maze-based Game, also how to organize code effectively. 

    * Using a *Game_grid.png* as a based background containing all information of location indicating by color.

2. Software Prototyping Programmimg hw5

    * Having an object of class *Board* passing as a parameter in constructor method of another class (i.e. Ghost) to detect possible move.

    * Implement *RowCol* as helper

---

### Credits

* Thanks to orginal Pac-man Game developed and released by Namco in 1980

* PNG Sprite sheet from [this link](http://labs.phaser.io/assets/games/pacman/)