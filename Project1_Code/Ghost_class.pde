public class Ghost extends Sprite
{
    private PVector loc;
    private int refreshDelay;
    private float spd;
    private Board board;
    private boolean dying;
    private boolean isHome;

    public Ghost (PApplet theApplet, String filename, int cols, int rows, int zOrder, Board b)
    {
        super(theApplet, filename, cols, rows, zOrder);
        setVisible(true);
        refreshDelay = 1000;
        loc = new PVector();
        spd = 0;
        board = b;
        dying = false;
        isHome = true;
    }

    public void setRefreshDelay(int rd) { 
        refreshDelay = rd; 
        spd = 1000*GSIZE/refreshDelay;
        setSpeed(spd);
    }

    public int getRefreshDelay() { return refreshDelay; }

    public void setLoc(int r, int c) { 
        loc = board.rowColToCoord(r, c); 
        setXY(loc.x, loc.y);
    }

    public void setNewLocation(float x, float y, int frame)
    {
        RowCol rc = board.coordToRowCol(x, y);
        loc = board.rowColToCoord(rc.row(), rc.col());
        if(rc.row() == 9 && rc.col() == 8) 
        {
            setDirection(-PI/2);
            setFrameSequence(4, 5, 0.2f);
            isHome = false;
        }
        if (loc.x == GSIZE/2 && (frame == 2 || frame == 3))
        {
            loc.x = 18.5*GSIZE;
        }
        if (loc.x == 18.5*GSIZE && (frame == 0 || frame == 1))
        {
            loc.x = GSIZE/2;
        }
        setXY(loc.x, loc.y);
    }

    public void isDying()
    {
        setSpeed(0);
        setFrameSequence(8, 12, 0.2f);
        dying = true;
    }

    public boolean getDying() { return dying; }

    public void run()
    {
        IntList pMove = new IntList();
        float getX = (float) getX();
        float getY = (float) getY();
        if (!board.isWall(getX+GSIZE, getY, isHome)) pMove.append(1);
        if (!board.isWall(getX-GSIZE, getY, isHome)) pMove.append(2);
        if (!board.isWall(getX, getY+GSIZE, isHome)) pMove.append(3);
        if (!board.isWall(getX, getY-GSIZE, isHome)) pMove.append(4);
        pMove.shuffle();
        int r = pMove.get(0);
        if (r == 1)
        {
            setFrameSequence(0, 1, 0.2f);
            setDirection(0);
        }
        else if (r == 2)
        {
            setFrameSequence(2, 3, 0.2f);
            setDirection(PI);
        }
        else if (r == 3)
        {
            setFrameSequence(6, 7, 0.2f);
            setDirection(PI/2);
        }
        else if (r == 4)
        {
            setFrameSequence(4, 5, 0.2f);
            setDirection(-PI/2);
        }
        setNewLocation(getX, getY, getFrame());
        pMove.clear();

    }
}