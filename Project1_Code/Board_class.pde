
public class Board
{
    private PImage img;
    private float w, h;
    private int length;

    public Board(String filename)
    {
        img = loadImage(filename);
        h = img.height;
        w = img.width;
        length = int(h/GSIZE);
    }

    public float getHeight() { return h; }
    public float getWidth() { return w; }
    public int getLength() { return length; }

    public boolean isWall(float x, float y, boolean isHome)
    {
        int c = img.get(int(x), int(y));
        if ( !isHome && c == color(255)) return true;
        return isWall(x, y);
    }

    public boolean isWall(float x, float y)
    {
        int c = img.get(int(x), int(y));
        if (c == color(0, 0, 255)) return true;
        return false;
    }

    public boolean isAvailable(int row, int col)
    {
        int c = img.get(int(row*GSIZE+GSIZE/2), int(col*GSIZE+GSIZE/2));
        if (c == color(0)) return true;
        return false;
    }

    public RowCol coordToRowCol (float x, float y)
    {
        return new RowCol(int(x/GSIZE), int(y/GSIZE));
    }

    public PVector rowColToCoord (int row, int col)
    {
        return new PVector(row*GSIZE+GSIZE/2, col*GSIZE+GSIZE/2);
    }

}

public class RowCol
{
    private int row, col;

    public RowCol (int r, int c)
    {
        row = r; col = c;
    }
    
    public int row() { return row; }
    public int col() { return col; }
    public void setRow(int r) { row = r; }
    public void setCol(int c) { col = c; }

}