
public class Fruit extends Sprite
{   
    private String name;
    private Board board;
    private PVector loc;

    public Fruit (PApplet theApplet, String filename, int zOrder, Board b)
    {
        super(theApplet, filename, zOrder);
        board = b;
    }

    public String getName() { return name; }

    public void setLoc(int r, int c)
    {
        loc = board.rowColToCoord(r, c); 
        setXY(loc.x, loc.y);
        setVisible(true);
    }
}