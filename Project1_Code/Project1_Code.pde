/*
	Project 1
	Name of Project: Pacman
	Author: Pimpisut Chanpanich
	Date: May 31, 2020
*/

import sprites.utils.*;
import sprites.maths.*;
import sprites.*;

StopWatch timer;
double[] times;
double[] runtimes;
Board board;
ArrayList<Sprite> walls;
ArrayList<Ghost> ghosts;
ArrayList<Fruit> fruits;
FactoryGhost fg;
FactoryFruit ff;

Pacman player;
PFont font;
int score;
int lives;
int numDead;
PImage pacman;
PImage bg;
PImage[] fruitImg;
int[] cntFruit;

final int SPLASH = 0;
final int INPLAY = 1;
final int END = 2;
int mode;
final float GSIZE = 30.0;

void setup()
{
    size(670, 670);
    timer = new StopWatch();
    font = createFont("emulogic.ttf", 20);
    textFont(font);

    mode = SPLASH;
    pacman = loadImage("Pacman_pic.png");
    bg = loadImage("Game_bg.png");

    fruitImg = new PImage[4];
    fruitImg[0] = loadImage("fruits/apple.png");
    fruitImg[1] = loadImage("fruits/cherry.png");
    fruitImg[2] = loadImage("fruits/melon.png");
    fruitImg[3] = loadImage("fruits/orange.png");

    board = new Board("Game_grid.png");
    walls = new ArrayList<Sprite>();

    ghosts = new ArrayList<Ghost>();
    fg = new FactoryGhost();

    fruits = new ArrayList<Fruit>();
    ff = new FactoryFruit();

    initGameStart();
}

void initGameStart()
{
    score = 0;
    lives = 3;
    numDead = 0;

    player = new Pacman(this, "Pacman_sheet.png", 21, 5, 100, board);

    ghosts.clear();
    ghosts.add (fg.Blinky(this, board));
    ghosts.add (fg.Pinky(this, board));
    ghosts.add (fg.Inky(this, board));
    ghosts.add (fg.Clyde(this, board));

    times = new double[ghosts.size()];
    runtimes = new double[ghosts.size()];

    for(int i = 0; i < ghosts.size(); i++) 
    {
        times[i] = (float)ghosts.get(i).getRefreshDelay()/1000;
        runtimes[i] = 0;
    }

    fruits.clear();
    cntFruit = new int[4];

    for (int i = 0; i < 2; i++)
    {
        fruits.add (ff.Apple(this, board));
        fruits.add (ff.Cherry(this, board));
        fruits.add (ff.Melon(this, board));
        fruits.add (ff.Orange(this, board));
    }

    IntList arrRows = new IntList();
    IntList arrCols = new IntList();
    
    for (int i = 0; i < 19; i++)
    {
        arrRows.append(i); arrCols.append(i);
    }

    for (Fruit fruit: fruits)
    {
        while(true)
            {
                arrRows.shuffle();
                arrCols.shuffle();
                if(board.isAvailable(arrCols.get(0), arrRows.get(0)))
                {
                    fruit.setLoc(arrCols.get(0), arrRows.get(0));
                    arrCols.remove(0);
                    arrRows.remove(0);
                    break;
                }
            }
    }
}

void draw()
{
    background(0);
    image(bg, width/2, height/2);
    
    infoDisplay();

    if (mode == INPLAY)
    {
        float deltaTime = (float) timer.getElapsedTime();
        updateAllSprites(deltaTime);
        processCollisions();

        // Thread
        float rt = (float) timer.getRunTime();
        for(int j = 0; j < times.length; j++)
        {
            if (rt > runtimes[j] && !ghosts.get(j).getDying())
            {
                ghosts.get(j).run();
                runtimes[j] += times[j];
            }
        }
    }

    pushMatrix();

    translate(50, 50);
    for (Fruit fruit: fruits) fruit.draw();
    for (Ghost ghost: ghosts) 
    {
        if (ghost.getFrame() == 12) ghost.setVisible(false);
        ghost.draw();
    }
    player.noPressed();
    if (player.getFrame()%21 == 20) player.setVisible(false);
    player.draw();

    popMatrix();

    if (mode == SPLASH)
    {
        text("GET READY!!", width/2-60, 280);
        text("PRESS'P'TO START", width/2-95, 400);
    }
    if (numDead == 4 && checkInvisible()) 
    {
        text("YOU WIN!", width/2-55, 280);
        text("PRESS'P'TO RESTART", width/2-105, 400);
        mode = END;
    }
    if (!player.isVisible() && lives > 0)
    {
        lives -= 1;
        player.reborn();
    }
    if (lives == 0 && !player.isVisible()) 
    {
        text("GAME OVER!", width/2-60, 280);
        text("PRESS'P'TO RESTART", width/2-105, 400);
        mode = END;
    }
}

boolean checkInvisible()
{
    for(Ghost ghost: ghosts)
    {
        if (ghost.isVisible()) return false;
    }
    return true;

}

void updateAllSprites(float deltaTime)
{
    for(Ghost gh: ghosts) gh.update(deltaTime);
    player.update(deltaTime);
}

void infoDisplay()
{
    textSize(20);
    fill(255);
    text("SCORE " + score, 50, 40);
    text("LIVES ", 50, 650);
    for (int i = 0; i < lives; i++) image(pacman, 180+35*i, 640, 30, 30);

    textSize(12);
    for (int i = 0; i < cntFruit.length; i++)
    {
        image(fruitImg[i], width/2+80*i-20, 30);
        text(cntFruit[i]+"/2", width/2+80*i ,40);
    }
}

void processCollisions()
{
    for(int i = 0; i < ghosts.size(); i++)
    {
        if (player.oo_collision(ghosts.get(i), 20) && !ghosts.get(i).getDying() && !player.getDying())
        {
            if (player.getState() == (i+1)%5) 
            {
                ghosts.get(i).isDying();
                score += 100;
                numDead += 1;
            }
            else player.isDying();
        }
    }

    for (Fruit fruit: fruits){
        if (player.oo_collision(fruit, 60) && fruit.isVisible())
        {
            fruit.setVisible(false);
            String name = fruit.getName();
            switch(name)
            {
                case "apple": 
                    cntFruit[0] += 1;
                    score += 20;
                    break;
                case "cherry": 
                    cntFruit[1] += 1;
                    score += 20;
                    break;
                case "melon": 
                    cntFruit[2] += 1;
                    score += 20;
                    break;
                case "orange": 
                    cntFruit[3] += 1;
                    score += 20;
                    break;
            }
        }
    }
}

void keyPressed() {
    if (key == 'p') 
    {
        if (mode == SPLASH || mode == END)
        {
            if (mode == END) initGameStart();
            mode = INPLAY;
            timer.reset();
        }
    }
    if (key == CODED && !player.getDying())
    {
        if (keyCode == UP) player.moveUp();
        else if (keyCode == DOWN) player.moveDown();
        else if (keyCode == LEFT) player.moveLeft();
        else if (keyCode == RIGHT) player.moveRight();
    }
    if (!player.getDying())
    {
        if (key == 'a' && cntFruit[0] == 2) player.setState(1);
        if (key == 's' && cntFruit[1] == 2) player.setState(2);
        if (key == 'd' && cntFruit[2] == 2) player.setState(3);
        if (key == 'f' && cntFruit[3] == 2) player.setState(4);
        if (key == 'v') player.setState(0);
    }
}

class FactoryGhost
{
    Ghost Blinky(PApplet theApplet, Board b)
    {
        Ghost gh = new Ghost(theApplet, "Blinky_sheet.png", 13, 1, 100, b);
        gh.setLoc(9, 9);
        gh.setFrameSequence(4, 5, 0.2f);
        gh.setRefreshDelay(875);
        gh.setDirection(-PI/2);
        return gh;
    }

    Ghost Pinky(PApplet theApplet, Board b)
    {
        Ghost gh = new Ghost(theApplet, "Pinky_sheet.png", 13, 1, 100, b);
        gh.setLoc(9, 9);
        gh.setFrameSequence(4, 5, 0.2f);
        gh.setRefreshDelay(500);
        gh.setDirection(-PI/2);
        return gh;
    }

    Ghost Inky(PApplet theApplet, Board b)
    {
        Ghost gh = new Ghost(theApplet, "Inky_sheet.png", 13, 1, 100, b);
        gh.setLoc(10, 9);
        gh.setFrameSequence(0, 1, 0.2f);
        gh.setRefreshDelay(1000);
        gh.setDirection(0);
        return gh;
    }

    Ghost Clyde(PApplet theApplet, Board b)
    {
        Ghost gh = new Ghost(theApplet, "Clyde_sheet.png", 13, 1, 100, b);
        gh.setLoc(8, 9);
        gh.setFrameSequence(2, 3, 0.2f);
        gh.setRefreshDelay(625);
        gh.setDirection(-PI);
        return gh;
    }
    
}

class FactoryFruit
{
    Fruit Apple(PApplet theApplet, Board b)
    {
        Fruit f = new Fruit(theApplet, "fruits/apple.png",200, b);
        f.name = "apple";
        return f;
    }

    Fruit Melon(PApplet theApplet, Board b)
    {
        Fruit f = new Fruit(theApplet, "fruits/melon.png",200, b);
        f.name = "melon";
        return f;
    }

    Fruit Orange(PApplet theApplet, Board b)
    {
        Fruit f = new Fruit(theApplet, "fruits/orange.png",200, b);
        f.name = "orange";
        return f;
    }

    Fruit Cherry(PApplet theApplet, Board b)
    {
        Fruit f = new Fruit(theApplet, "fruits/cherry.png",200, b);
        f.name = "cherry";
        return f;
    }
}