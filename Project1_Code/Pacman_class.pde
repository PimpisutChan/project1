public class Pacman extends Sprite
{
    private Board board;
    private PVector loc;
    private float speed;
    private int nrow;
    private boolean dying;

    public Pacman(PApplet theApplet, String filename, int cols, int rows, int zOrder, Board b)
    {
        super(theApplet, filename, cols, rows, zOrder);
        board = b;
        loc = board.rowColToCoord(4,13);
        nrow = 0;
        speed = 60;
        reborn();
    }

    public void reborn()
    {
        setXY(loc.x, loc.y);
        setVisible(true);
        setFrameSequence(0, 1, 0.2f);
        setSpeed(speed, -PI/2);
        setState(0);
        setDying(false);
    }

    public boolean getDying() { return dying; }
    public void setDying(boolean dying) { this.dying = dying; }
    public void isDying()
    {
        setSpeed(0);
        setFrameSequence(nrow*21+8, nrow*21+20, 0.2f);
        setDying(true);
    }

    public int getState() { return nrow;}
    public void setState(int num) { 
        nrow = num; 
        int newFrame = getFrame()%21 + nrow*21;
        if ((newFrame%21)%2 == 0) setFrameSequence(newFrame, newFrame+1);
        else setFrameSequence(newFrame-1, newFrame);
    }

    public boolean isBound()
    {
        float x = (float) getX();
        float y = (float) getY();
        int frame = getFrame()%21;
        int bound = (int)GSIZE/2-3;
        if (x > board.getWidth()) setX(0);
        else if (x < 0) setX(board.getWidth());

        if((frame == 6 || frame == 7) && (board.isWall(x+GSIZE/2, y-bound) || board.isWall(x+GSIZE/2, y+bound))) { return true; }
        else if((frame == 0 || frame == 1) && (board.isWall(x-bound, y-GSIZE/2) || board.isWall(x+bound, y-GSIZE/2))) { return true; }
        else if((frame == 2 || frame == 3) && (board.isWall(x-bound, y+GSIZE/2) || board.isWall(x+bound, y+GSIZE/2))) { return true; }
        else if((frame == 4 || frame == 5) && (board.isWall(x-GSIZE/2, y-bound) || board.isWall(x-GSIZE/2, y+bound))) { return true; }
        return false;
    }

    public void noPressed()
    {
        if(isBound()) setSpeed(0);
    }

    public void moveUp()
    {
        setSpeed(speed, -PI/2);
        setFrameSequence(nrow*21, nrow*21+1, 0.2f);
        noPressed();
    }

    public void moveDown()
    {
        setSpeed(speed, PI/2);
        setFrameSequence(nrow*21+2, nrow*21+3, 0.2f);
        noPressed();
    }

    public void moveRight()
    {
        setSpeed(speed, 0);
        setFrameSequence(nrow*21+6, nrow*21+7, 0.2f);
        noPressed();
    }

    public void moveLeft()
    {
        setSpeed(speed, -PI);
        setFrameSequence(nrow*21+4, nrow*21+5, 0.2f);
        noPressed();
    }
}